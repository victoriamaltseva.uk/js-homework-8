/* Теоретический вопрос

1. Опишите своими словами, как Вы понимаете, что такое обработчик событий.

## Задание

Создать поле для ввода цены с валидацией. 
Задача должна быть реализована на языке javascript, 
без использования фреймворков и сторонник библиотек (типа Jquery).

#### Технические требования:
- При загрузке страницы показать пользователю поле ввода (`input`) с надписью `Price`. 
Это поле будет служить для ввода числовых значений
- Поведение поля должно быть следующим:
   - При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
   - Когда убран фокус с поля - его значение считывается, над полем создается `span`, 
   в котором должен быть выведен текст: `Текущая цена: ${значение из поля ввода}`. 
   Рядом с ним должна быть кнопка с крестиком (`X`). 
   Значение внутри поля ввода окрашивается в зеленый цвет.
   - При нажатии на `Х` - `span` с текстом и кнопка `X` должны быть удалены. 
   Значение, введенное в поле ввода, обнуляется.
   - Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, 
   под полем выводить фразу - `Please enter correct price`. `span` со значением при этом не создается.
- В папке `img` лежат примеры реализации поля ввода и создающегося `span`.
*/


const input = document.createElement('input');
input.setAttribute('placeholder', 'Price');
input.classList.add('input');
document.body.append(input);

const price = document.createElement('span');
price.classList.add('price');
document.body.prepend(price);

const btn = document.createElement('button');
btn.innerText = 'x';
btn.classList.add('btn');

btn.addEventListener('click', (event) => {
    price.innerHTML = '';
    input.value = '';
});

const error = document.createElement('span');
document.body.append(error);


input.addEventListener('blur', (event) => {
    // Обнуляем спаны
    input.classList.remove('input-error');
    error.innerHTML = ''
    price.innerHTML = '';

    event.target.classList.add('input-text');
    const value = event.target.value;

    if (value > 0) {         
        price.innerHTML = `Текущая цена: ${value}`;
        price.append(btn);
    } else {
        input.classList.add('input-error');
        error.innerHTML = `Please enter correct price`;
    }  
});








